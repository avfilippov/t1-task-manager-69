package ru.t1.avfilippov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.entity.dto.CustomUser;

@Controller
public final class ProjectsController {

    @Autowired
    private ProjectDTOService projectService;


    @GetMapping("/projects")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
    }

}
