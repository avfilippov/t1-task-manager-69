package ru.t1.avfilippov.tm.entity.dto;

public class Result {

    private Boolean success = true;

    public String message = "";

    public Result() {
    }

    public Result(Exception exception) {
        this.success = false;
        message = exception.getMessage();
    }

    public Result(Boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
