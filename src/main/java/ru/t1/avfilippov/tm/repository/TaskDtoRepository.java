package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;
import java.util.Optional;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {

    Optional<TaskDto> findByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    List<TaskDto> findAllByUserId(String userId);

}
