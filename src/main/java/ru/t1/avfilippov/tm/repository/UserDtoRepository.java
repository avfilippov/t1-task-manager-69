package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.UserDto;
import ru.t1.avfilippov.tm.entity.model.User;


public interface UserDtoRepository extends JpaRepository<UserDto, String> {

    UserDto findByLogin (String login);

}
