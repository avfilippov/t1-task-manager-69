package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.entity.dto.ProjectDto;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;

import java.util.List;
import java.util.Optional;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    Optional<ProjectDto> findByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    List<ProjectDto> findAllByUserId(String userId);

}
