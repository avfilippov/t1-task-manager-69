package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.entity.dto.TaskDto;
import ru.t1.avfilippov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDTOService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    @Secured({"ROLE_ADMINISTRATOR","ROLE_USER"})
    public List<TaskDto> findAll() {
        return service.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDto save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        return service.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        service.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        service.delete(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDto> tasks
    ) {
        service.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public void clear() {
        service.clear();
    }

}
